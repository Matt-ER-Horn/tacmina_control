// Tacmina Pump control
// Commands: 
// ON1 : turns on pump 1
// ON2 : turns on pump2
// SP1 : turns off pump 1
// SP2 : turns off pump 2
// FL1X.X : sets flow rate of pump 1, X.X is a number with or without decimal
// FL2X.X : sets flow rate of pump 2, X.X is a number with or without decimal
// CA1X : Calibration values for flow 1, X is an integer between 0 and 255
// CA2X : Calibration values for flow 2, X is an integer between 0 and 255


#include <Arduino.h>
#define DAC1 25
#define DAC2 26
char buff[32];
char command[4];
float cmd_value = 0;
#define CMD_LEN 3
float dacValue1 = 0;
float dacValue2 = 0;
int CA1 = 0;
int CA2= 0;
float flow1 = 0;
float flow2 = 0;
bool pump1On = false;
bool pump2On = false;

void setup() {
    Serial.begin(115200);
    dacWrite(DAC1, dacValue1);
    dacWrite(DAC2, dacValue2);
}

void loop() {
//Serial Commands for operating valve----------------------------------
if(Serial.available()>0)
 {
    uint8_t num_chars = Serial.readBytesUntil(
      0x0D, // Reads until it reaches terminal carriage return
      buff,
      sizeof(buff)
    );
    buff[num_chars] = '\0';
    sscanf(
      buff,
      "%3s %f",
      command,
      &cmd_value
    );
    command[3] = '\0';
  

  if (strncmp(command, "ON1", CMD_LEN)==0)// Turn pump 1 on
  {     
    dacWrite(DAC1, dacValue1);
    pump1On = true;
  }

  else if(strncmp(command, "ON2", CMD_LEN)==0) // Turn pump 2 on
  {   
    dacWrite(DAC2, dacValue2);
    pump2On = true;
  }
  else if (strncmp(command, "SP1", CMD_LEN)==0)// Turn pump 1 of
  {     
    dacWrite(DAC1, 0);
    pump1On = false;
  }

  else if(strncmp(command, "SP2", CMD_LEN)==0) // Turn pump 2 on to Value 1
  {   
    dacWrite(DAC2, 0);
    pump2On = false;
  }

  else if(strncmp(command, "FL1", CMD_LEN)==0) // Set pump 1 flow rate
  {   flow1 = cmd_value;
      if(flow1 >= 0.01 && flow1 <= 10.0)
      {
      dacValue1 = (flow1 * 13.564) + (47.364);
      if(pump1On == true)
      {
        dacWrite(DAC1, dacValue1);
      }
      //Serial.println(dacValue1);
      Serial.print("pump 1 set to ");
      Serial.print(flow1,1);
      Serial.println(" ml/min");
      }
      else(Serial.println("flow out of Range"));
  }
    else if(strncmp(command, "FL2", CMD_LEN)==0) // Set pump 2 flow rate
  {   flow2 = cmd_value;
      if(flow2 >= 0.01 && flow2 <= 10.0)
      {
      dacValue2 = (flow2 * 13.564) + (47.364);
       if(pump2On == true)
      {
        dacWrite(DAC2, dacValue2);
      }
      Serial.print("pump 2 set to ");
      Serial.print(flow2,1);
      Serial.println(" ml/min");
      }
      else(Serial.println("flow out of Range"));
  }
    else if(strncmp(command, "CA1", CMD_LEN)==0) // Accepts a DAC value between 0 and 255 to use for calibration
  {  
    CA1 = cmd_value; 
    dacWrite(DAC1, CA1);
    Serial.println(CA1);
  }
      else if(strncmp(command, "CA2", CMD_LEN)==0) // Accepts a DAC value between 0 and 255 to use for calibration
  {  
    CA2 = cmd_value; 
    dacWrite(DAC2, CA2);
    Serial.println(CA2);
  }
 }
}    